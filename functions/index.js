const functions = require("firebase-functions");
const admin = require('firebase-admin');
const stripe = require('stripe')(functions.config().stripe.secret_key);
const LocaleCurrency = require('locale-currency');

admin.initializeApp();
admin.firestore().settings({ignoreUndefinedProperties:true});

// User account methods

exports.onUserSignUp = functions.auth.user().onCreate(user => {

    const userDoc = admin.firestore().collection('users').doc(user.uid);

    userDoc.set({
        display_name: user.displayName
    });

    return stripe.customers.create({
        name: user.displayName,
        email: user.email,
        metadata: {
            firebaseUID: user.uid
        }
    })
    .then(customer => {

        userDoc.update({
            stripe_customer_id: customer.id
        });
    })
    .catch(error => console.error(error));
});

exports.onUserDelete = functions.auth.user().onDelete(user => {

    const userDoc = admin
        .firestore()
        .collection('users')
        .doc(user.uid);

    admin
        .firestore()
        .collection('passes')
        .where('user_reference', '==', userDoc)
        .get()
        .then(snapshot => {
            const batch = admin.firestore().batch();
            snapshot.forEach(v => batch.delete(v.ref));
            batch.commit();
        });

    return userDoc.delete();
});

// Stripe methods

exports.stripeCreatePaymentIntent = functions.https.onRequest(async(req, res) => {
    
    const user_uid = req.query.user_uid
    const fitness_center_id = req.query.fitness_center_id
    const event_id = req.query.event_id

    try {

        if (user_uid == null) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'Provide "user_uid" parameter'
            );
        }

        if (fitness_center_id == null && event_id == null) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'Provide either "fitness_center_id" or "event_id" parameter'
            );
        }

        const userDoc = await admin.firestore().collection('users').doc(user_uid).get();
        const stripeCustomerId = userDoc.data().stripe_customer_id;

        if (!userDoc.exists || stripeCustomerId == null) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'Could not find "stripe_customer_id" for uid=' + user_uid  
            );
        }

        const activityCollectionName = fitness_center_id != null ? 'fitness_centers' : 'events'
        const activityDocumentValue = fitness_center_id != null ? fitness_center_id : event_id
        const activityDoc = await admin.firestore().collection(activityCollectionName).doc(activityDocumentValue).get();
        const activityPrice = activityDoc.data().price
        const activityLocale = activityDoc.data().locale_identifier
        const activityCurrency = LocaleCurrency.getCurrency(activityLocale)

        if (!activityDoc.exists || activityPrice == null || Number(activityPrice) <= 0) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'Could not find "price" for ' + activityCollectionName + ' ' + activityDocumentValue  
            );
        }

        if (activityCurrency == null) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'Could not find "locale_identifier" for ' + activityCollectionName + ' ' + activityDocumentValue  
            );
        }

        const paymentIntent = await stripe.paymentIntents.create({
            amount: Math.floor(activityPrice * 100),
            currency: LocaleCurrency.getCurrency(activityLocale),
            customer: stripeCustomerId,
            confirmation_method: 'automatic',
            capture_method: 'automatic',
            metadata: {
                firebaseUID: user_uid,
                fitness_center_id: fitness_center_id,
                event_id: event_id
            }
        });

        return res.json(paymentIntent);
    } catch(error) {

        return res.status(303).json({
            message: error
        });
    }
});

exports.handleStripeWebhookEvents = functions.https.onRequest(async (req, resp) => {

    const relevantEvents = new Set([
        'payment_intent.succeeded',
        'payment_intent.processing',
        'payment_intent.payment_failed',
        'payment_intent.canceled',
    ]);

    let event;

    try {

        event = stripe.webhooks.constructEvent(
            req.rawBody,
            req.headers['stripe-signature'],
            functions.config().stripe.webhook_secret_key
        );
    } catch (error) {

        console.error('❗️ Webhook Error: Invalid Secret');

        return resp.status(401).send('Webhook Error: Invalid Secret');
    }

    if (relevantEvents.has(event.type)) {

        try {

            switch (event.type) {
                case 'payment_intent.succeeded':
                case 'payment_intent.processing':
                // case 'payment_intent.payment_failed':
                // case 'payment_intent.canceled':
                    const id = event.data.object.id;
                    await updatePaymentRecord(id);
                    break;
                default:
                    throw new Error('Unhandled relevant event!');
            }
        } catch (error) {

            console.error(
                `❗️ Webhook error for [${event.data.object.id}]`,
                error.message
            );

            return resp.status(400).send('Webhook handler failed.');
        }
    }

    return resp.json({ received: true });
});

const updatePaymentRecord = async (id) => {

    const payment = await stripe.paymentIntents.retrieve(id);
    const firebase_uid = payment.metadata.firebaseUID;
    const fitness_center_id = payment.metadata.fitness_center_id;
    const event_id = payment.metadata.event_id;

    let userReference
    if (firebase_uid != null) {

        userReference = admin.firestore().collection('users').doc(firebase_uid)
    } else {

        throw new Error('User not found!');
    }

    let fitnessCenterReference
    if (fitness_center_id != null) {
        fitnessCenterReference = admin.firestore().collection('fitness_centers').doc(fitness_center_id)
    }

    let eventReference
    let eventSnapshot
    if (event_id != null) {
        eventReference = admin.firestore().collection('events').doc(event_id)
        eventSnapshot = await eventReference.get()
    }

    let endDate
    if (eventSnapshot != null && eventSnapshot.data().date != null) {

        endDate = eventSnapshot.data().date.toDate()
        endDate.setDate(endDate.getDate() + 1);
        endDate.setUTCHours(0, 0, 0, 0);
    } else {

        endDate = new Date();
        endDate.setMonth(endDate.getMonth() + 1)
        endDate.setUTCHours(0, 0, 0, 0);
    }

    await admin
        .firestore()
        .collection('passes')
        .doc()
        .set({
            'payment_id': payment.id,
            'user_reference': userReference,
            'fitness_center_reference': fitnessCenterReference,
            'event_reference': eventReference,
            'end_date': endDate
        });
  };